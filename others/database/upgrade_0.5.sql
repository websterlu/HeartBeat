SET NAMES utf8;


-- 2015-05-01
-- ###############
-- Domain:  WeixinUser
-- ###############
DROP TABLE IF EXISTS weixin_user;
CREATE TABLE `weixin_user` (
  `id`          INT(11)      NOT NULL AUTO_INCREMENT,
  `guid`        VARCHAR(255) NOT NULL UNIQUE,
  `create_time` DATETIME,
  `archived`    TINYINT(1)            DEFAULT '0',
  `version`     INT(11)               DEFAULT 0,

  `openid`      VARCHAR(255) UNIQUE,
  `hb_username` VARCHAR(255),
  `nick_name`   VARCHAR(255),
  PRIMARY KEY (`id`),
  INDEX `guid_index` (`guid`),
  INDEX `hb_username_index` (`hb_username`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8;


-- ###############
-- Domain:  ApplicationInstanceWeixinUser
-- ###############
DROP TABLE IF EXISTS application_instance_weixin_user;
CREATE TABLE `application_instance_weixin_user` (
  `id`             INT(11)      NOT NULL AUTO_INCREMENT,
  `guid`           VARCHAR(255) NOT NULL UNIQUE,
  `create_time`    DATETIME,
  `archived`       TINYINT(1)            DEFAULT '0',
  `version`        INT(11)               DEFAULT 0,

  `instance_id`    INT(11),
  `weixin_user_id` INT(11),
  PRIMARY KEY (`id`),
  INDEX `guid_index` (`guid`),
  INDEX `instance_id_index` (`instance_id`),
  INDEX `weixin_user_id_index` (`weixin_user_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8;


ALTER TABLE monitoring_reminder_log ADD `openid` VARCHAR(255);
ALTER TABLE monitoring_reminder_log ADD `wechat_content` TEXT;
